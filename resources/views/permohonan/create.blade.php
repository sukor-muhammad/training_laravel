@extends('layouts.land')

@section('title','create')

@section('content')

@php
    // use App\Helpers\CustomHelpers;
@endphp

{{-- <form  method="POST" enctype="multipart/form-data">
<input type="text" name=''>

</form> --}}


@if ($errors->has('error'))
<div class="alert alert-danger">
    {{ $errors->first('error') }}
</div>
@endif

<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">Create Complaint</h4>
                </div>
                <div class="card-body">
                    <form  action="{{route('permohonan.store')}}"  method="POST"  enctype="multipart/form-data">
 
                        @csrf


                        <div class="row">
    <div class="col-md-4">
        <div class="input-group input-group-static mb-4">
            <label>Title</label>
            <select class="form-control" name='complainant_title'>
                <option value="">Sila pilih</option>
                @foreach (App\Helpers\CustomHelpers::getTitle() as $item)
                    <option value='{{ $item }}' {{ (old('complainant_title',$data->complainant_title??'')??'') == $item ? 'selected' : '' }}>{{ $item }}</option>
                @endforeach
            </select>
        </div>
        @error('complainant_title')
            <span class="text-danger">{{ $message }}</span>
        @enderror
    </div>
    <div class="col-md-8">
        <div class="input-group input-group-static mb-4">
            <label>Name</label>
            <input type="text" class="form-control" name="complainant_name" value="{{ old('complainant_name',$data->complainant_name??'') }}">
        </div>
        @error('complainant_name')
            <span class="text-danger">{{ $message }}</span>
        @enderror
    </div>
</div>

{{-- input line 2 --}}


<div class="row">
    <div class="col-md-4">
        <div class="input-group input-group-static mb-4">
            <label>Nationality</label>
            <select class="form-control" name='complainant_nationality'>
                <option value="">Sila pilih</option>
                @foreach (App\Helpers\CustomHelpers::getNationality() as $key2=> $item2)
                    <option value='{{ $key2 }}' 
                    {{ (old('complainant_nationality',$data->complainant_nationality??'')??'') == $key2 ? 'selected' : '' }}>{{ $item2 }}</option>
                @endforeach
            </select>
        </div>
        @error('complainant_nationality')
            <span class="text-danger">{{ $message }}</span>
        @enderror
    </div>
    <div class="col-md-8">
        <div class="input-group input-group-static mb-4">
            <label>Mykad/Paspport</label>
            <input type="text" class="form-control" name="complainant_identity" value="{{ old('complainant_identity',$data->complainant_identity??'') }}">
        </div>
        @error('complainant_identity')
            <span class="text-danger">{{ $message }}</span>
        @enderror
    </div>
</div>

{{-- end input line 2 --}}

{{-- input line 3 --}}

<div class="row">

    <div class="col-md-12">
        <div class="input-group input-group-static mb-4">
            <label>Complaint</label>
            <input type="text" class="form-control" name="complainant_complaint" value="{{ old('complainant_complaint',$data->complainant_complaint??'') }}">
        </div>
        @error('complainant_complaint')
            <span class="text-danger">{{ $message }}</span>
        @enderror
    </div>

</div>

{{-- input line 3 --}}


{{-- input line 4 --}}

<div class="row">

    <div class="col-md-12">
        <div class="input-group input-group-static mb-4">
            <label>file 1</label>
            <input type="file" class="form-control" name="file_upload[]" >
        </div>
        @error('file_upload')
            <span class="text-danger">{{ $message }}</span>
        @enderror
    </div>

</div>

<div class="row">

    <div class="col-md-12">
        <div class="input-group input-group-static mb-4">
            <label>file 2</label>
            <input type="file" class="form-control" name="file_upload[]" >
        </div>
        @error('file_upload')
            <span class="text-danger">{{ $message }}</span>
        @enderror
    </div>

</div>

{{-- input line 4 --}}
                    

                        <button type="submit" class="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>




@endsection

