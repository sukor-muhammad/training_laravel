@extends('layouts.app')


@section('content')

{{-- form carian --}}

<form >
    <div class="row">
        <div class="col-md-4">

<div class="input-group input-group-outline mb-4">
    <label class="form-label">Nama</label>
    <input name='name' type="text" class="form-control">
  </div>
</div>
</div>


  <button class='btn btn-info' type='submit'>Cari</button>

</form>

{{-- end form carian --}}


<table class='table'>

    @foreach ($state as $item)
 
    <tr>
     <td>{{$item->name}}</td>
     
    </tr>
         
 
    
     @endforeach
     
     
 </table>

 {{ $state->appends(request()->except('page'))->links() }}

@endsection
