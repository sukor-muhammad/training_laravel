@extends('layouts.app')
@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h4>View Customer</h4>
                    </div>
                    <div class="card-body">
                    
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="input-group input-group-outline my-3">
                                        <label class="form-label">Name</label>
                                        <input type="text" id="name" name="name" value="{{ $customer['name'] }}" class="form-control">
                                    </div>
                                    
                                </div>
                                <div class="col-md-6">
                                    <div class="input-group input-group-outline my-3">
                                        <label class="form-label">Type</label>
                                        <input type="text" id="type" name="type" value="{{ $customer['type'] }}" class="form-control">
                                    </div>
                                    
                                </div>
                                <div class="col-md-6">
                                    <div class="input-group input-group-outline my-3">
                                        <label class="form-label">Email</label>
                                        <input type="text" id="email" name="email" value="{{ $customer['email'] }}" class="form-control">
                                    </div>
                                    
                                </div>
                                <div class="col-md-6">
                                    <div class="input-group input-group-outline my-3">
                                        <label class="form-label">Address</label>
                                        <input type="text" id="address" name="address" value="{{ $customer['address'] }}" class="form-control">
                                    </div>
                                    
                                </div>
                                <div class="col-md-6">
                                    <div class="input-group input-group-outline my-3">
                                        <label class="form-label">City</label>
                                        <input type="text" id="city" name="city" value="{{ $customer['city'] }}" class="form-control">
                                    </div>
                                    
                                </div>
                                <div class="col-md-6">
                                    <div class="input-group input-group-outline my-3">
                                        <label class="form-label">State</label>
                                        <input type="text" id="state" name="state" value="{{ $customer['state'] }}" class="form-control">
                                    </div>
                                    
                                </div>
                            </div>
                            
                    </div>
                    <div class="card-footer"></div>
                </div>
            </div>
        </div>
    </div>
@endsection