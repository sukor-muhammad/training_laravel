@extends('layouts.app')
@section('content')
    @if ($message = Session::get('success'))
        <div class="alert alert-success text-white fade show">
            <span class="alert-text">{{ $message }}</span>
        </div>
    @elseif($message = Session::get('error'))
        <div class="alert alert-danger text-white fade show">
            <span class="alert-text">{{ $message }}</span>
        </div>
    @endif
    <a class="btn btn-success float-end" 
    href="{{ route('customer.create') }}">Create</a>
    <table class="table">
        <thead>
            <th>#</th>
            <th>Name</th>
            <th>Email</th>
            <th>Address</th>
            <th>Action</th>
        </thead>
        <tbody>
            @foreach($paginator as $key => $customer)
            <tr>
                <td>{{ $key + $paginator->firstItem() }}</td>
                <td>{{ $customer['name'] }}</td>
                <td>{{ $customer['email'] }}</td>
                <td>{{ $customer['address'] }}</td>
                <td>
                    <a class="btn btn-info" 
                    href="{{ route('customer.show', $customer['id']) }}">
                        View</a>
                    <a class="btn btn-warning" 
                    href="{{ route('customer.edit', $customer['id']) }}">
                        Edit</a>
                    <form method="POST" 
                    action="{{ route('customer.destroy', $customer['id']) }}">
                        @csrf
                        @method('DELETE')
                        <button type="submit" class="btn btn-danger">Delete</button>
                    </form>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
    {{ $paginator->links() }}
@endsection