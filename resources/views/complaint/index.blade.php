@extends('layouts.app')

@section('title','test 2')

@section('content')
    <p>This is my body content ini test saja.</p>



    @if (session('success'))
    <div class="alert alert-success">
        {{ session('success') }}
    </div>
@endif



<form >
    <div class="row">
        <div class="col-md-4">

<div class="input-group input-group-outline mb-4">
    <label class="form-label">Nama</label>
    <input name='complainant_name' type="text" class="form-control">
  </div>
</div>
</div>

<div class="row">
    <div class="col-md-4">

<div class="input-group input-group-outline mb-4">
<label class="form-label">Status</label>
<select  class="form-control" name="complainant_status" id="complainant_status" >
@foreach ($statusLookup as $statuskey=>$statusitem)
    <option value="{{$statuskey}}">{{$statusitem}}</option>
@endforeach

</select>

</div>
</div>
</div>



  <button class='btn btn-info' type='submit'>Cari</button>

</form>




<table class='table'>

   @foreach ($modelComp as $item)

   <tr>
    <td>{{$item->complainant_name}}</td>
    <td>{{$item->statusCom->name??''}}</td>
    <td>{{$item->nationalityAsset->name??''}}</td>
    <td>    
        <a  class='btn btn-info' href="{{route('admin.complaint.edit',$item)}}">update</a>

        <form method="POST" action="{{ route('admin.complaint.destroy', $item) }}">
            @csrf
            @method('DELETE')
            <button type="submit" class="btn btn-danger">Delete</button>
        </form>

    </td>
   </tr>
        

   
    @endforeach
    
    
</table>

{{ $modelComp->appends(request()->except('page'))->links() }}

@endsection