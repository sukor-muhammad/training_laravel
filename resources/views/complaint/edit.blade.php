@extends('layouts.app')

@section('title','create')

@section('content')

@php
    // use App\Helpers\CustomHelpers;
@endphp

{{-- <form  method="POST" enctype="multipart/form-data">
<input type="text" name=''>

</form> --}}


@if ($errors->has('error'))
<div class="alert alert-danger">
    {{ $errors->first('error') }}
</div>
@endif

<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">Create Complaint</h4>
                </div>
                <div class="card-body">
                    <form  action="{{route('admin.complaint.update',['complaint'=>$data])}}"  method="POST"  enctype="multipart/form-data">
                   
                        @csrf
                        @method('PUT')

                        @include('complaint._form')
                    

                        <button type="submit" class="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>




@endsection

