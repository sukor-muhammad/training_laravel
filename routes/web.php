<?php

use App\Models\AssetFile;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\StateController;
use App\Http\Controllers\ComplaintController;
use App\Http\Controllers\CustomerController;
use App\Http\Controllers\PermohonanController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Route::get('/test/test2', function () {
    echo 'test';
});


// Route::get('/permohonan', function () {

//     return view('permohonan.index');
    
// });

Route::get('/permohonan/index',[PermohonanController::class,'index']);


Route::get('/permohonan/create',[PermohonanController::class,'create'])->name('permohonan.create');

Route::post('/permohonan/store',[PermohonanController::class,'store'])->name('permohonan.store');

Route::get('/permohonan/success/{complainant}',[PermohonanController::class,'success'])->name('permohonan.success');





Route::get('report/view-image/{fileid}', function($fileid){
    $source = AssetFile::find($fileid);
    if($source){
        $file = public_path('/storage/'.$source->location);
    }else{
        $file = public_path('/storage/uploads/report/no_image.jpg');
    }

    return response()->file($file);
})->name('download.file'); 


Route::prefix('admin')->group(function () {

    
    Route::name('admin.')->group(function () {
        
        Route::get('/permohonan/update/{id?}',[PermohonanController::class,'update'])->name('permohonan.update');

        Route::get('/state',[StateController::class,'index'])->name('state.index');

        Route::resource('complaint', ComplaintController::class);

    });



});


Route::resource('customer', CustomerController::class);







