<?php

namespace App\Http\Controllers;

use App\Models\AssetFile;
use Illuminate\Http\Request;
use App\Helpers\CustomHelpers;
use App\Models\ComplainantDetail;
use App\Mail\ComplaintApplication;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Auth\Events\Validated;

class PermohonanController extends Controller
{


    public function index(){

    $datasearch=request(['complainant_name','complainant_status']);

      $data['modelComp']=  $modelComp =
       ComplainantDetail::with(['statusCom','nationalityAsset'])
       ->latest()
       ->filter($datasearch)
       ->paginate(10);


       $data['statusLookup']=  CustomHelpers::getComplaintStatus();
  
        return view('permohonan.index',$data);
    }



    public function create(){



     return view('permohonan.create');


    }

    public function store(Request $request){


        
  
        // $input=$request->all();
        // $input['complainant_status']='STS001';


        // $complainant=  ComplainantDetail::create($input);
        // $complainant= null;

       $validated= $request->
       validate([
        'complainant_name'=>'required',
        'complainant_title'=>'required',
        'complainant_nationality'=>'required',
        'complainant_identity'=>'required',
        'complainant_complaint'=>'required',
        'file_upload'=>'nullable|max:2048'
         ],['complainant_name.required'=>'Sila masukan nama anda']);


         $validated['complainant_status']='STS001';

   
       $complainant=  ComplainantDetail::create($validated);



    //    save file
       if($complainant->id ){

        if($request->file_upload){

        foreach($request->file_upload as $key=>$file){

            

            $filename="doc_$key".time().'com'.$complainant->id.'.'.$file->getClientOriginalExtension();
          
            // dd($file);
            $filepath=$file->storeAs('uploads/complainant',$filename,'public');

            
            if($file??''){

                $asset=new AssetFile();
                $asset->name=$filename;
                $asset->location=$filepath;
                $asset->ext=$file->getMimeType();
                $asset->category='file_complainant';
                $asset->reference_id=$complainant->id;
                $asset->reference_type='complainant_details';
                $asset->save();
                 

                

            }//if2

            

        }//forecah1

    }
      
            $emailto=$complainant->complainant_email??'recipient@example.com';


           Mail::to($emailto)
           ->cc('sukor@gmail.com')
           ->send(new ComplaintApplication($complainant));

        

            return redirect()->route('permohonan.success',$complainant)->with('success', 'Complainant created successfully.');

       }else{

        return redirect()->route('permohonan.create')->withErrors(['error' => 'Complainant created No successfully.']);

       }

    //    end save file



    }


    public function success(ComplainantDetail $complainant){


        $data['data']=$complainant;

        return view('permohonan.success',$data);



    }

    


    // public function update($id=''){
        

    //     $d['id']=$id;
    //     $d['name']='nama saya ali';
    //     // dd($id);


    //     return view('permohonan.update',$d);
    // }


    public function update($id=''){
        

     
        $name='nama saya ali';
        // dd($id);


        return view('permohonan.update',compact('id','name'));
    }




    
}
