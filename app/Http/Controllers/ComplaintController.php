<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Helpers\CustomHelpers;
use App\Models\ComplainantDetail;
use App\Mail\ComplaintApplication;
use Illuminate\Support\Facades\Mail;

class ComplaintController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $datasearch=request(['complainant_name','complainant_status']);

        $data['modelComp']=  $modelComp =
         ComplainantDetail::with(['statusCom','nationalityAsset'])
         ->latest()
         ->filter($datasearch)
         ->paginate(10);
  
  
         $data['statusLookup']=  CustomHelpers::getComplaintStatus();
    
          return view('complaint.index',$data);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {

        return view('complaint.create');
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $validated= $request->
        validate([
         'complainant_name'=>'required',
         'complainant_title'=>'required',
         'complainant_nationality'=>'required',
         'complainant_identity'=>'required',
         'complainant_complaint'=>'required',
         'file_upload'=>'nullable|max:2048'
          ],['complainant_name.required'=>'Sila masukan nama anda']);
 
 
          $validated['complainant_status']='STS001';
 
    
        $complainant=  ComplainantDetail::create($validated);
 
 
 
     //    save file
        if($complainant->id ){
 
         if($request->file_upload){
 
         foreach($request->file_upload as $key=>$file){
 
             
 
             $filename="doc_$key".time().'com'.$complainant->id.'.'.$file->getClientOriginalExtension();
           
             // dd($file);
             $filepath=$file->storeAs('uploads/complainant',$filename,'public');
 
             
             if($file??''){
 
                 $asset=new AssetFile();
                 $asset->name=$filename;
                 $asset->location=$filepath;
                 $asset->ext=$file->getMimeType();
                 $asset->category='file_complainant';
                 $asset->reference_id=$complainant->id;
                 $asset->reference_type='complainant_details';
                 $asset->save();
                  
 
                 
 
             }//if2
 
             
 
         }//forecah1
 
     }
       
             $emailto=$complainant->complainant_email??'recipient@example.com';
 
 
            Mail::to($emailto)
            ->cc('sukor@gmail.com')
            ->send(new ComplaintApplication($complainant));
 
         
 
             return redirect()->route('permohonan.success',$complainant)->with('success', 'Complainant created successfully.');
 
        }else{
 
         return redirect()->route('permohonan.create')->withErrors(['error' => 'Complainant created No successfully.']);
 
        }
 
     //    end save file
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(ComplainantDetail $complaint)
    {

        $data['data']=$complaint;

        return view('complaint.edit',$data);
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, ComplainantDetail $complaint)
    {

         $validated= $request->
       validate([
        'complainant_name'=>'required',
        'complainant_title'=>'required',
        'complainant_nationality'=>'required',
        'complainant_identity'=>'required',
        'complainant_complaint'=>'required',
        'file_upload'=>'nullable|max:2048'
         ],['complainant_name.required'=>'Sila masukan nama anda']);

         $complainant=$complaint;
        

   
         $complainant->update($validated);



    //    save file
       if($complainant->id ){

        if($request->file_upload){

        foreach($request->file_upload as $key=>$file){

            

            $filename="doc_$key".time().'com'.$complainant->id.'.'.$file->getClientOriginalExtension();
          
            // dd($file);
            $filepath=$file->storeAs('uploads/complainant',$filename,'public');

            
            if($file??''){

                $asset=new AssetFile();
                $asset->name=$filename;
                $asset->location=$filepath;
                $asset->ext=$file->getMimeType();
                $asset->category='file_complainant';
                $asset->reference_id=$complainant->id;
                $asset->reference_type='complainant_details';
                $asset->save();
                 

                

            }//if2

            

        }//forecah1

    }
      
           

        

            return redirect()->route('admin.complaint.index',$complainant)->with('success', 'Complainant created successfully.');

       }else{
        // {{route('admin.complaint.edit',$item)}}
        return redirect()->route('admin.complaint.edit',$complainant)->withErrors(['error' => 'Complainant created No successfully.']);

       }


    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(ComplainantDetail $complaint)
    {
        $complaint->delete();

        return redirect()->route('admin.complaint.index')
        ->with('success', 'Complainant has been deleted successfully.');
    }
}
