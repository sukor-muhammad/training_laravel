<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use Illuminate\Pagination\LengthAwarePaginator;

class CustomerController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        // $curl = curl_init();
        // curl_setopt_array($curl, [
        //     CURLOPT_URL => 'http://amtis.opengw.net:8089/l10api/public/api/v1/customer?'.http_build_query(['page' => request('page') ?? null]),
        //     CURLOPT_RETURNTRANSFER => true,
        //     CURLOPT_CUSTOMREQUEST => 'GET'
        // ]);
        // $response = curl_exec($curl);
        // curl_close($curl);
        // dd($response);
        // $data = json_decode($response, true);
        // dd($data);
        $response = Http::get('http://amtis.opengw.net:8089/l10api/public/api/v1/customer', ['page' => request('page') ?? null]);
        $data = $response->json();
        $paginator = new LengthAwarePaginator(
            collect($data['data']),
            $data['meta']['total'],
            $data['meta']['per_page'],
            $data['meta']['current_page'],
            ['path' => route('customer.index')]
        );
        // dd($data);
        return view('customer.index', compact('paginator'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view('customer.create');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $validated = $request->validate([
            'name' => 'required',
            'email' => 'required',
            'type' => 'required',
            'address' => 'required',
            'state' => 'required',
            'city' => 'required',
        ],[
            'name.required' => 'nama diperlukan'
        ]);
        $response = Http::post('http://amtis.opengw.net:8089/l10api/public/api/v1/customer', $validated);
        if($response->successful()){
            return to_route('customer.index')->withSuccess('Customer Created');
        }
        // dd($response->json());
        return back()->withErrors($response->json()['errors'])
        ->withInput();
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        $response = Http::get("http://amtis.opengw.net:8089/l10api/public/api/v1/customer/$id");
        if($response->successful()){
            $customer = $response->json()['data'];
            return view('customer.show', compact('customer'));
        }
        return to_route('customer.index')
        ->withError('Something Wrong!');
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        $response = Http::get("http://amtis.opengw.net:8089/l10api/public/api/v1/customer/$id");
        if($response->successful()){
            $customer = $response->json()['data'];
            return view('customer.edit', compact('customer'));
        }
        return to_route('customer.index')
        ->withError('Something Wrong!');
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        $validated = $request->validate([
            'name' => 'required',
            'email' => 'required',
            'type' => 'required',
            'address' => 'required',
            'state' => 'required',
            'city' => 'required',
        ],[
            'name.required' => 'nama diperlukan'
        ]);
        $response = Http::put("http://amtis.opengw.net:8089/l10api/public/api/v1/customer/$id", $validated);
        if($response->successful()){
            return to_route('customer.index')->withSuccess('Customer Updated');
        }
        dd($response->json());
        return back()->withErrors($response->json()['errors'])
        ->withInput();
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        $response = Http::delete("http://amtis.opengw.net:8089/l10api/public/api/v1/customer/$id");
        if($response->successful()){
            return to_route('customer.index')
            ->withSuccess('Customer Deleted!');
        }
        return to_route('customer.index')
        ->withError('Something wrong!');
    }
}
