<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class State extends Model
{
    use HasFactory;


    public function scopeFilter($query,$filter){

        $query->when($filter['name']??false,function ($query,$search){
            
            $query->where('name','like',"%$search%");
    
        });
    
        // $query->when($filter['complainant_status']??false,function ($query,$search){
            
        //     $query->where('complainant_status','=',"$search");
    
        // });
    }



}
