<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class ComplainantDetail extends Model
{
    use HasFactory;

    protected $table = 'complainant_details';

    protected $primaryKey = 'id';

    protected $fillable = 
    ['complainant_name',
    'complainant_title',
    'complainant_nationality',
    'complainant_identity',
    'complainant_name',
    'complainant_complaint',
    'complainant_status'
];



    public function statusCom(): BelongsTo
{
    return $this->belongsTo(AssetLookup::class, 
    'complainant_status', 'code');
}

public function nationalityAsset(): BelongsTo
{
    return $this->belongsTo(AssetLookup::class, 
    'complainant_nationality', 'code');
}


public function scopeFilter($query,$filter){

    $query->when($filter['complainant_name']??false,function ($query,$search){
        
        $query->where('complainant_name','like',"%$search%");

    });

    $query->when($filter['complainant_status']??false,function ($query,$search){
        
        $query->where('complainant_status','=',"$search");

    });


}


public function fileAsset():HasMany 
{
    return $this->hasMany(AssetFile::class, 
    'reference_id', 'id')->where(['reference_type'=>'complainant_details']);
}




}
