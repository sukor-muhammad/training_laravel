<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use App\Models\ComplainantDetail;
use Illuminate\Mail\Mailables\Content;
use Illuminate\Queue\SerializesModels;
use Illuminate\Mail\Mailables\Envelope;
use Illuminate\Contracts\Queue\ShouldQueue;

class ComplaintApplication extends Mailable
{
    use Queueable, SerializesModels;

    public $name;
    public ComplainantDetail $complaint;

    /**
     * Create a new message instance.
     */
    public function __construct(ComplainantDetail $comp)
    {
        $this->complaint = $comp;
       
    }

    /**
     * Get the message envelope.
     */
    public function envelope(): Envelope
    {
        return new Envelope(
            subject: 'Complaint Application',
        );
    }

    /**
     * Get the message content definition.
     */
    public function content(): Content
    {

        
        return new Content(
            markdown: 'mails.complaintApplication',
            with: ['complaint' => $this->complaint]
        );
    }

    /**
     * Get the attachments for the message.
     *
     * @return array<int, \Illuminate\Mail\Mailables\Attachment>
     */
    public function attachments(): array
    {
        return [];
    }
}
