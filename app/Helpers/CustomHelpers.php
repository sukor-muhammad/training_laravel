<?php
namespace App\Helpers;

use App\Models\AssetLookup;

class  CustomHelpers{

    
   public static function getComplaintStatus(){

    $data=AssetLookup::where(['category'=>'complaint_status'])->get();

    $plucked=$data->pluck('name','code');

    return $plucked;

    }


    public static function getNationality(){

        $data=AssetLookup::where(['category'=>'type_citizen'])->get();
    
        $plucked=$data->pluck('name','code');
    
        return $plucked;
    
        }


    public static function getTitle(){


        $d=['Mr.',
        'Mrs.',
       ' Prof.',
       ' Dr.',
      '  Miss',
        'Ms.'];

        return $d;

        
    }


    
}